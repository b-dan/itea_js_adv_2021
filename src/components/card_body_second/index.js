import React from "react";
import CardBodyFirst from "../card_body_first";
import "./card_body_second.css"

function CardBodySecond({data}) {
    console.log(data);
    return(
        <>
            <table className="my-table">
                <thead>
                    <tr>
                        <th>Задача</th>
                        <th>Статус</th>
                        <th>Дії</th>
                    </tr>
                </thead>
                <tbody>
                    {Array.isArray(data)&&data.length>0?
                    data.map(function (element, i){
                        return (
                            <tr key={i}>
                                <td>{element.title}</td>
                                <td>{element.completed?<span>Задача выполнена</span>:<span>Задача не выполнена</span>}</td>
                                <td>
                                    <button className="btn-status">Статус</button>
                                    <button className="btn-delete">Удалить</button>
                                </td>
                            </tr>
                        )
                    })
                    :
                    <tr><td colSpan="3">Задачі вдсутні</td></tr>
                    }
                    
                </tbody>
            </table>
        </>
    )
}

export default CardBodySecond;