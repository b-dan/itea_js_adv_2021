import React from "react";
import './card.css';
import CardBodyFirst from "../card_body_first"
import CardBodySecond from "../card_body_second"

function Card({title, cardbody, data}) {
    return(
        <>
        <div className="my-card">
            <div className="my-title">{title}</div>
            <div className="my-card-body">
                {cardbody==='first'?<CardBodyFirst/>:<CardBodySecond data={data}/>}
            </div>
        </div>
        </>
    )
}

export default Card;