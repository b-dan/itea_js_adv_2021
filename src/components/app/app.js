import React from "react";
import Card from "../card";
import data from "../../data/data";

function App() {
    return(<>
        <Card title='Створення нової задачі' cardbody='first'/>
        <Card title='Список задач' cardbody='second' data={data}/>
    </>
    )
}
export default App