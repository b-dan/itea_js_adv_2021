import React from "react";
import "./card_body_first.css"

function CardBodyFirst() {
    return(
        <>
        <p>Створіть нову задачу&#128526;</p>
        <div>
            <input className="my-input" placeholder="Напишіть назву задачі"></input>
        </div>
        <div>
            <button className="btn">Зберегти</button>
        </div>
        </>
    )
}
export default CardBodyFirst;